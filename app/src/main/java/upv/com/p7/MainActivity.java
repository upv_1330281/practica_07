package upv.com.p7;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnLlamar, btnMensaje;
    EditText numero, smsBody, numero2;
    TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabHost= (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tab1 = tabHost.newTabSpec("tab1");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("tab2");

        tab1.setIndicator("Llamada");
        tab1.setContent(R.id.tab_1);

        tab2.setIndicator("Mensaje");
        tab2.setContent(R.id.tab_2);

        tabHost.addTab(tab1);
        tabHost.addTab(tab2);

        numero = (EditText) findViewById(R.id.numero);
        numero2 = (EditText) findViewById(R.id.numero2);
        smsBody = (EditText) findViewById(R.id.smsBody);
        btnLlamar = (Button) findViewById(R.id.btnLlamar);
        btnMensaje = (Button) findViewById(R.id.btnMensaje);

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String uri = "tel:"+numero.getText().toString();
                    Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
                    startActivity(callIntent);
                }catch(Exception e) {
                    Toast.makeText(getApplicationContext(),"Error en llamada",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("smsto:" + numero2.getText().toString());
                Intent smsSIntent = new Intent(Intent.ACTION_SENDTO, uri);
                smsSIntent.putExtra("sms_body", smsBody.getText().toString());
                try{
                    startActivity(smsSIntent);
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Fallo al enviar",
                            Toast.LENGTH_LONG).show();
                    ex.printStackTrace();
                }
            }
        });
    }

}
